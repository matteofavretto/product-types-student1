package operations;

import java.util.SortedMap;
import java.util.TreeMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Operations {
	
	HashMap<String, ProductType> productTypes = new HashMap<String, ProductType>();
	HashMap<String, Customer> customers = new HashMap<String, Customer>();

//R1
	public int addProductType(String productType, int n, int price) throws OException {
		if(productTypes.containsKey(productType))
			throw new OException("Duplicate product");
		productTypes.put(productType, new ProductType(productType, n, price));
		return n * price;
	}

	public int getNumberOfProducts(String productType) throws OException {
		if(!productTypes.containsKey(productType))
			throw new OException("Missing product");
		return productTypes.get(productType).getAvailableQuantity();
	}

	public SortedMap<Integer, List<String>> groupingProductTypesByPrices() {
		HashMap<Integer, List<String>> priceMap = new HashMap<>();
        for (HashMap.Entry<String, ProductType> entry : productTypes.entrySet()) {
            String productName = entry.getKey();
            Integer price = entry.getValue().price;
            Integer quantity = entry.getValue().availableQuantity;
            if (quantity == null || quantity == 0) {
                // Ignore products with no available quantities
                continue;
            }
            List<String> products = priceMap.computeIfAbsent(price, k -> new ArrayList<>());
            products.add(productName);
        }
        
        // Sort the lists of product names alphabetically
        for (List<String> products : priceMap.values()) {
            Collections.sort(products);
        }
        
        // Create a new TreeMap to sort the hashmap by price and return it as a SortedMap
        SortedMap<Integer, List<String>> sortedMap = new TreeMap<>();
        sortedMap.putAll(priceMap);
        return sortedMap;
	}

//R2
	public int addDiscount(String customer, int discount) {
		Customer c = customers.get(customer);
		if(c == null)
			customers.put(customer,  new Customer(customer, discount));
		else 
			c.addDiscount(discount);
		return customers.get(customer).getDiscount();
	}

	public int customerOrder(String customer, String ptpn, int discount) throws OException {
		Customer c = customers.get(customer);
		int total = 0;
		if(c == null)
			throw new OException("Non-existing customer");
		
		if(discount > customers.get(customer).getDiscount())
			throw new OException("Exceeded available discount");
		
		HashMap<String, Integer> order = new HashMap<>();
        String[] parts = ptpn.split("\\s+");
        for (String part : parts) {
            String[] pair = part.split(":");
            String name = pair[0];
            int quantity = Integer.parseInt(pair[1]);
            order.put(name, quantity);
        }
        
        for(HashMap.Entry<String, Integer> entry : order.entrySet()) {
        	String prodName = entry.getKey();
        	int quantity = entry.getValue();
        	if(!productTypes.containsKey(prodName))
        		throw new OException("Missing product");
        	if(quantity > productTypes.get(prodName).getAvailableQuantity())
        		return 0;
        	productTypes.get(prodName).sellProducts(quantity);
        	total += quantity * productTypes.get(prodName).getPrice();
        }
        
        c.addDiscount(-1 * discount);
		return total - discount;
	}

	public int getDiscountAvailable(String customer) {
		return customers.get(customer).getDiscount();
	}

//R3
	public int evalByCustomer(String customer, String productType, int score) throws OException {
		// An exception is thrown if the customer:
		// has not purchased any product of the type indicated,
		// has already given a score to the product type indicated, or the score is out
		// of range.
		return 0;
	}

	public int getScoreFromProductType(String customer, String productType) throws OException {
		return 0;
	}

	public SortedMap<Integer, List<String>> groupingCustomersByScores(String productType) {
		return null;
	}

//R4
	public SortedMap<Integer, List<String>> groupingCustomersByNumberOfProductsPurchased() {
		//groups the customers by increasing number of products purchased. 
		//The customers are listed in alphabetical order.
		return null;
	}

	public SortedMap<String, Integer> largestExpenseForCustomer() {
		//provides the largest expense for each customer (in increasing order)
		return null;
	}

}
