package operations;

public class Customer {
	String name;
	int discount;
	
	public Customer(String name, int discount) {
		this.name = name;
		this.discount = discount;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void addDiscount(int discount) {
		this.discount += discount;
	}
	
	public int getDiscount() {
		return this.discount;
	}
}
