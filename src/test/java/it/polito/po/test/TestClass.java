package it.polito.po.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.SortedMap;

import org.junit.Test;

import operations.*;

public class TestClass {
	
	Operations op = new Operations();
	
	@Test
	public void testAddProducts() throws OException {
		int totalPrice1 = op.addProductType("chairA", 10, 12);
		int totalPrice2 = op.addProductType("deskA", 4, 20);
		int totalPrice3 = op.addProductType("chairB", 10, 15);
		int totalPrice4 = op.addProductType("bedA", 2, 50);
		
		assertEquals(120, totalPrice1);
		assertEquals(80, totalPrice2);
		assertEquals(150, totalPrice3);
		assertEquals(100, totalPrice4);
	}
	
	@Test
	public void testGetProducts() throws OException {
		op.addProductType("chairA", 10, 12);
		op.addProductType("deskA", 4, 20);
		
		assertEquals(10, op.getNumberOfProducts("chairA"));
		assertEquals(4, op.getNumberOfProducts("deskA"));
	}
	
	@Test
	public void testGroupProductsByPrice() throws OException {
		op.addProductType("chairA", 10, 12);
		op.addProductType("deskA", 4, 20);
		op.addProductType("chairB", 10, 12);
		op.addProductType("bedA", 2, 50);
		
		SortedMap<Integer, List<String>> map1 = op.groupingProductTypesByPrices();
		String expected = "{12=[chairA, chairB], 20=[deskA], 50=[bedA]}";
		assertEquals(expected, map1.toString());
	}
	
	@Test
	public void testAddDiscountToCustomers() {
		assertEquals(10, op.addDiscount("c1", 10));
		assertEquals(20, op.addDiscount("c1", 10));
	}
	
	@Test
	public void testCustomerOrders() throws OException {
		op.addProductType("sofa", 10, 150);
		op.addDiscount("c1", 20);
		String order = "sofa:2";
		
		int expense = op.customerOrder("c1", order, 10);
		assertEquals(expense, 290);
		
		op.addProductType("chair", 3, 50);
		op.addDiscount("c2", 20);
		String order2 = "chair:2 sofa:1";
		
		int expense2 = op.customerOrder("c2", order2, 20);
		assertEquals(expense2, 230);
	}
	
	@Test
	public void testOrderTooManyProducts() throws OException {
		op.addProductType("sofa", 10, 150);
		op.addDiscount("c1", 20);
		String order = "sofa:12";
		
		int expense = op.customerOrder("c1", order, 10);
		assertEquals(expense, 0);
	}
	
	@Test(expected=OException.class)
	public void testOrderExceedDiscount() throws OException {
		op.addProductType("sofa", 10, 150);
		op.addDiscount("c1", 20);
		String order = "sofa:12";
		
		int expense = op.customerOrder("c1", order, 30);
	}
}
